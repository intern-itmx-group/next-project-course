var express = require("express");
var app = express();
var users = require("./users.json");
var fs = require("fs");
var path = require("path");
var usersDataFilePath = path.join(__dirname, "users.json");
const TokenManager = require("./tokenmanager");
// const bodyParser = require("body-parser");
// app.use(bodyParser.json());

app.use(express.json()); //For JSON requests
app.use(express.urlencoded({ extended: true }));
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  next();
});

app.get("/users", function (req, res) {
  const isAccess = TokenManager.checkAuthentication(req);

  if (isAccess) {
    console.log(users);
    res.send(users);
  }
});

app.get("/users/:id", function (req, res) {
  const isAccess = TokenManager.checkAuthentication(req);
  // var aUser = users[req.params.id];
  console.log("req ->", req.params.id);
  if (isAccess) {
    var aUser = users.filter((_, index) => {
      return index + 1 == req.params.id;
    });
    console.log(aUser);
    res.send(aUser);
  }
});

app.get("/users/checkrole/:id", function (req, res) {
  const userId = req.params.id; // Get the user ID from the request

  // Find the user with the matching ID in the users array
  const foundUser = users.find((user) => user.id === userId);

  console.log("userId:", userId);
  console.log("foundUser:", foundUser);

  if (foundUser) {
    // If the user is found, check their role
    if (foundUser.role === "admin") {
      res.json({
        status: true,
      });
    } else {
      res.json({
        status: false,
      });
    }
  } else {
    // If no user is found with the given ID
    res.status(404).json({
      message: "User not found",
    });
  }
});

app.get("/users/find/:userId", function (req, res) {
  const isAccess = TokenManager.checkAuthentication(req);

  if (isAccess) {
    var aUser = users.filter((user) => {
      return user.id == req.params.userId;
    });
    var oneUser = aUser[0];
    //console.log(aUser);
    res.send(oneUser);
  }
});
// var aUser = users[req.params.id];
// console.log("req ->", req.params.username);
// var aUser = users.filter((user) => {
//   return user.username == req.params.username;
// });
// var oneUser = aUser[0];
// console.log(aUser);
// res.send(oneUser);
// });

app.post("/login", (req, res) => {
  const { username, password } = req.body;
  // users.map((user) => {
  //   console.log(user);
  // });
  // Find user in the users data
  const user = users.find(
    (user) => user.username === username && user.password === password
  );
  if (user) {
    // User found, send success response
    let token = TokenManager.getGenerateAccessToken({
      username: username,
    });
    console.log("token -> ", token);
    res.status(200).json({
      status: "0",
      message: "Login successful",
      user,
      access_token: token.access_token,
      expired: token.expired,
    });
  } else {
    // User not found, send error response
    res.status(401).json({
      status: "1",
      message: "Invalid username or password",
    });
  }
});

app.post("/check_authen", (req, res) => {
  let jwtStatus = TokenManager.checkAuthentication(req);
  if (jwtStatus != false) {
    res.send(jwtStatus);
  } else {
    res.send(false);
  }
});

app.post("/get_user_data", (req, res) => {
  let jwtStatus = TokenManager.checkAuthentication(req);
  if (jwtStatus != false) {
    res.send(
      users.find((user) => {
        return user.username == jwtStatus.username;
      })
    );
  } else {
    res.send(false);
  }
});

// Signup endpoint
app.post("/signup", (req, res) => {
  const { username, password, email, role } = req.body;
  const imgurl =
    "https://i.pinimg.com/564x/4a/ca/6f/4aca6fdd35b62296dcf6d79ada4d95e0.jpg";

  // Check if the username already exists
  console.log(username);
  console.log("--------------------", users);
  const existingUser = false;

  if (existingUser) {
    res.status(409).json({ message: "Username already exists" });
  } else {
    // Generate a unique userId in the format "user_#"
    let maxNumber = 0;
    users.forEach((user) => {
      const numberPart = parseInt(user.id.split("_")[1]);
      if (!isNaN(numberPart) && numberPart > maxNumber) {
        maxNumber = numberPart;
      }
    });

    const userId = `user_${maxNumber + 1}`;

    // Add new user to the users data
    const newUser = { id: userId, username, password, email, role, imgurl };
    users.push(newUser);

    // Write the updated user data to the JSON file
    fs.writeFile(usersDataFilePath, JSON.stringify(users), (err) => {
      if (err) {
        res.status(500).json({ message: "Error writing to file" });
      } else {
        res.status(201).json({ message: "Signup successful", user: newUser });
      }
    });
  }
});

app.delete("/users/:userId", function (req, res) {
  const userIdToDelete = req.params.userId; // Get the userId from the request parameters

  // Find the index of the user with the matching userId in the users array
  const userIndex = users.findIndex((user) => user.id === userIdToDelete);

  if (userIndex !== -1) {
    // If the user is found, remove them from the array
    const newUserArray = [...users];
    newUserArray.splice(userIndex, 1);

    console.log("After delete -> ", newUserArray);

    // Write the updated user array to the file
    fs.writeFile(usersDataFilePath, JSON.stringify(newUserArray), (err) => {
      if (err) {
        res.status(500).json({ message: "Error writing to file" });
      } else {
        res
          .status(201)
          .json({ message: "Delete successful", users: newUserArray });
      }
    });
  } else {
    // If no user is found with the given userId
    res.status(404).json({ message: "User not found" });
  }
});

app.put("/users/update/:userId", function (req, res) {
  const userIdToUpdate = req.params.userId;
  const updatedUser = req.body;

  // Check if the username already exists
  const existingUser = users.find(
    (user) =>
      user.username === updatedUser.username && user.id !== userIdToUpdate
  );

  if (existingUser) {
    res.status(409).json({ message: "Username already exists" });
  } else {
    // Find the user index based on the userId
    const userIndex = users.findIndex((user) => user.id === userIdToUpdate);

    // Check if the user with the given userId exists
    if (userIndex !== -1) {
      // Update the user at the specified index
      users[userIndex] = { ...updatedUser };
      fs.writeFile(usersDataFilePath, JSON.stringify(users), (err) => {
        if (err) {
          console.log(err);
          res.status(500).json({ message: "Error writing to file" });
        } else {
          res.status(200).json({
            message: "User updated successfully",
            user: users[userIndex],
          });
        }
      });
    } else {
      res.status(404).json({ message: "User not found" });
    }
  }
});

app.post("/signout", (req, res) => {
  // const { token } = req.body;
  res.status(200).json({ message: "Sign out successful" });
});

//products part

app.get("/get_products", (req, res) => {
  try {
    const rawData = fs.readFileSync("products.json");
    const products = JSON.parse(rawData);
    res.json(products);
  } catch (err) {
    res.status(500).json({ error: "Failed to fetch products." });
  }
});

function generateProductID() {
  return Math.floor(10000000 + Math.random() * 90000000);
}

// Function to save products array to a JSON file
function saveProductsToFile(products) {
  const data = JSON.stringify(products, null, 2);
  fs.writeFileSync("products.json", data);
}

// Endpoint to add a new product
app.post("/add_product", (req, res) => {
  console.log("add product call!");
  const { product_name, product_detail, product_price } = req.body;

  if (!product_name || !product_detail || !product_price) {
    return res.status(400).json({
      error:
        "Invalid data. Please provide product_name, product_detail, and product_price.",
    });
  }

  const productID = generateProductID();
  const product = {
    product_id: productID,
    product_name,
    product_detail,
    product_price,
  };

  try {
    // Read the existing products data from the file
    const rawData = fs.readFileSync("products.json");
    const existingProducts = JSON.parse(rawData);

    // Append the new product to the existing data
    existingProducts.push(product);

    // Save the updated products array to the JSON file
    saveProductsToFile(existingProducts);

    res.json({ message: "Product added successfully.", product_id: productID });
  } catch (err) {
    res.status(500).json({ error: "Failed to add product." });
  }
});

var server = app.listen(3080, function () {
  var port = server.address().port;
  console.log("Application is running at http://127.0.0.1:%s", port);
});
