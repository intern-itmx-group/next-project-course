import { withAuth, NextRequestWithAuth } from "next-auth/middleware";
import { NextResponse } from "next/server";
import { API_URL } from "./config";

export default withAuth(
  function middleware(request: NextRequestWithAuth) {
    console.log("[Middleware] -> ", request.nextUrl.pathname);
    console.log("[Middleware] -> ", request.nextauth.token?.access);

    if (
      request.nextUrl.pathname.startsWith("/user") &&
      request.nextauth.token?.user?.role !== "admin" &&
      request.nextauth.token?.user?.role !== "user"
    ) {
      return NextResponse.rewrite(new URL("/denied", request.url));
    }
    if (
      request.nextUrl.pathname.startsWith("/admin") &&
      request.nextauth.token?.user?.role !== "admin"
    ) {
      return NextResponse.rewrite(new URL("/denied", request.url));
    }
    if (request.nextUrl.pathname.startsWith("/data")) {
      return NextResponse.rewrite(
        new URL(
          `${API_URL}:${process.env.NEXT_PUBLIC_MYPORT}/add_product`,
          request.url
        )
      );
    }
  },
  {
    callbacks: {
      authorized: ({ token }) => !!token,
    },
  }
);

export const config = {
  matcher: ["/home", "/admin", "/user", "/product", "/data/:path*"],
};
