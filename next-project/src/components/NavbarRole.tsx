"use client";
import React, { useEffect, useState } from "react";
import Link from "next/link";
import { signOut } from "next-auth/react";

import { navigateList_admin, navigateList_user } from "@/constant/navigateList";

type Props = {
  role: string;
};

const NavbarRole = (props: Props) => {

  const navigateList = props.role==='admin' ? navigateList_admin : navigateList_user;
  

  return (
    <>
      {/* {isAdmin ? <div>Hello admin</div> : <div>You don't have access</div>} */}
      
      <nav className="bg-white px-7 py-2 border-b-[2px] border-gray-700 grid justify-items-center">
        <ul className="flex">
          {navigateList.map((item,index) => (
            <li className="mr-6" key={index}>
              <Link href={item.path}>
                <button
                  className={`${
                    item.navName == "sign out"
                      ? "text-red-600"
                      : " text-gray-800"
                  } bg-transparent  font-bold py-1 px-4 rounded hover:bg-gray-200  focus:text-white  focus:bg-black`}
                  onClick={() => {
                    if (item.navName === "sign out") signOut();
                  }}
                >
                  {item.navName}
                </button>
              </Link>
            </li>
          ))}
        </ul>
      </nav>
    </>
  );
};

export default NavbarRole;
