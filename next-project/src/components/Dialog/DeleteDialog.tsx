"use client";
import React from "react";
import { useState } from "react";
import {
  Button,
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
  Typography,
} from "@material-tailwind/react";

interface Idata {
  id: number;
  username?: string;
  password?: string;
  role?: string;
  imgurl?: string;
  email?: string;
}

interface DeleteDialog {
  open: boolean;
  handleOpenDeleteDialog: () => void;
  selectDelete: Idata;
}
function DeleteDialog(props: DeleteDialog) {
  const handleDelete = () => {
    //handle save action
  };
  const [newUser, setNewUser] = useState<Idata>({
    username: props.selectDelete.username || "",
    password: props.selectDelete.password || "",
    role: props.selectDelete.role || "",
    id: props.selectDelete.id || 0,
    imgurl:
      props.selectDelete.imgurl ||
      "https://i.pinimg.com/564x/4a/ca/6f/4aca6fdd35b62296dcf6d79ada4d95e0.jpg",
    email: props.selectDelete.email || "",
  });

  return (
    <>
      <Dialog open={props.open} handler={props.handleOpenDeleteDialog}>
        <div className="flex items-center justify-between">
          <DialogHeader>
            <Typography color="red" variant="h4">
              Are you sure to delete this account?
            </Typography>
          </DialogHeader>
        </div>
        <DialogFooter className="space-x-2">
          <Button
            variant="outlined"
            color="red"
            onClick={props.handleOpenDeleteDialog}
          >
            cancle
          </Button>
          <Button
            variant="gradient"
            color="white"
            onClick={props.handleOpenDeleteDialog}
          >
            yes
          </Button>
        </DialogFooter>
      </Dialog>
    </>
  );
}

export default DeleteDialog;
