"use client";
import React from "react";
import { useState } from "react";
import {
  Button,
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
  Input,
  Select,
  Option,
} from "@material-tailwind/react";

interface Idata {
  id: number;
  username?: string;
  password?: string;
  role?: string;
  imgurl?: string;
  email?: string;
}

interface EditDialog {
  open: boolean;
  handleOpenEditDialog: () => void;
  selectEdit: Idata;
}
function EditDialog(props: EditDialog) {
  const handleSave = () => {
    //handle save here
  };
  const [newUser, setNewUser] = useState<Idata>({
    username: props.selectEdit.username || "",
    password: props.selectEdit.password || "",
    role: props.selectEdit.role || "",
    id: props.selectEdit.id || 0,
    imgurl:
      props.selectEdit.imgurl ||
      "https://i.pinimg.com/564x/4a/ca/6f/4aca6fdd35b62296dcf6d79ada4d95e0.jpg",
    email: props.selectEdit.email || "",
  });

  return (
    <>
      <Dialog open={props.open} handler={props.handleOpenEditDialog}>
        <div className="flex items-center justify-between">
          <DialogHeader>Edit</DialogHeader>
        </div>
        <DialogBody divider>
          <div className="grid gap-6">
            <Input
              label="Username"
              value={newUser.username}
              onChange={(event) => {
                setNewUser((prevData) => ({
                  ...prevData,
                  username: event.target.value,
                }));
              }}
            />
            <Input
              label="email"
              type="email"
              value={newUser.email}
              onChange={(event) => {
                setNewUser((prevData) => ({
                  ...prevData,
                  email: event.target.value,
                }));
              }}
            />
            <Input
              label="Image"
              value={newUser.imgurl}
              onChange={(event) => {
                setNewUser((prevData) => ({
                  ...prevData,
                  imgurl: event.target.value,
                }));
              }}
            />
            <Select 
              label="Role"
              value={newUser.role}
              onChange={ (event) => {
                console.log('select -> ',event )
                setNewUser((prevData) => ({
                  ...prevData,
                  role: event,
                }));
              }}
            >
              <Option value={'admin'}>admin</Option>
              <Option value={'manger'}>manger</Option>
              <Option value={'user'}>user</Option>
            </Select>
          </div>
        </DialogBody>
        <DialogFooter className="space-x-2">
          <Button
            variant="outlined"
            color="red"
            onClick={props.handleOpenEditDialog}
          >
            cancle
          </Button>
          <Button
            variant="gradient"
            color="green"
            onClick={props.handleOpenEditDialog}
          >
            save
          </Button>
        </DialogFooter>
      </Dialog>
    </>
  );
}

export default EditDialog;
