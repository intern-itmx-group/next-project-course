"use client";
import React, { useEffect } from "react";
import {
  Button,
  Dialog,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Input,
  Checkbox,
  Alert,
} from "@material-tailwind/react";
import axios from "axios";
import { API_URL } from "@/config";

type Props = {};
type Product = {
  product_id?: number;
  product_name?: string;
  product_detail?: string;
  product_price?: number;
};

const ProductDialog = (props: Props) => {
  const [open, setOpen] = React.useState(false);
  const [openAlert, setOpenAlert] = React.useState(false);
  const [openAlertSuccess, setOpenAlertSuccess] = React.useState(false);
  const [product, setProduct] = React.useState<Product | undefined>();
  const handleOpen = () => setOpen((cur) => !cur);
  const handleAddProduct = async () => {
    console.log("Product -> ", product);
    if (
      product?.product_name === undefined ||
      product?.product_price === undefined ||
      product?.product_detail === undefined
    ) {
      setOpenAlert((cur) => !cur);
    } else {
      try {
        const response = await axios.post(`/data/add_product`, product);
        // const response = await axios.post(
        //   `${API_URL}:${process.env.NEXT_PUBLIC_MYPORT}/add_product`,
        //   product
        // );
        window.location.reload();
        console.log("Response:", response.data);
        setOpenAlertSuccess((cur) => !cur);
      } catch (error) {
        console.error("Error:", error);
      }
    }
    setOpen((cur) => !cur);
  };
  useEffect(() => {
    const timer = setTimeout(() => {
      setOpenAlert(false);
    }, 3000);
    return () => clearTimeout(timer);
  }, [openAlert]);

  return (
    <>
      <div className="w-full flex justify-end">
        <Button onClick={handleOpen} className="mb-4">
          Add product
        </Button>
      </div>
      <Alert color="red" open={openAlert} onClose={() => setOpenAlert(false)}>
        Please provide all product information to add product
      </Alert>
      <Alert
        color="green"
        open={openAlertSuccess}
        onClose={() => setOpenAlertSuccess(false)}
      >
        Add product success
      </Alert>
      <Dialog
        size="xs"
        open={open}
        handler={handleOpen}
        className="bg-transparent shadow-none"
      >
        <Card className="mx-auto w-full max-w-[24rem]">
          <CardHeader
            variant="gradient"
            color="blue"
            className="mb-4 grid h-28 place-items-center"
          >
            <Typography variant="h3" color="white">
              Add Product
            </Typography>
          </CardHeader>
          <CardBody className="flex flex-col gap-4">
            <Input
              label="Product Name"
              size="lg"
              required
              onChange={(e) =>
                setProduct({ ...product, product_name: e.target.value })
              }
            />
            <Input
              label="Product Price"
              size="lg"
              type="number"
              required
              onChange={(e) =>
                setProduct({
                  ...product,
                  product_price: Number(e.target.value),
                })
              }
            />
            <Input
              label="Product Detail"
              size="lg"
              required
              onChange={(e) =>
                setProduct({ ...product, product_detail: e.target.value })
              }
            />
          </CardBody>
          <CardFooter className="pt-0">
            <Button variant="gradient" onClick={handleAddProduct} fullWidth>
              Add Product
            </Button>
          </CardFooter>
        </Card>
      </Dialog>
    </>
  );
};

export default ProductDialog;
