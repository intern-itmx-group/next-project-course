"use client";
import React, { useEffect, useState } from "react";
import Link from "next/link";

type Props = {};

const Navbar = (props: Props) => {
  const [isAdmin, setIsAdmin] = useState(false);
  // useEffect(() => {
  //   // use userid to be params for test using user_2 for user , user_1 for admin
  //   const userId = "user_2";
  //   fetch(`/api/fetch/checkrole/${userId}`)
  //     .then((response) => response.json())
  //     .then((data) => setIsAdmin(data.status))
  //     .catch((error) => console.error("Error fetching fake data:", error));
  // }, []);

  return (
    <>
      {/* {isAdmin ? <div>Hello admin</div> : <div>You don't have access</div>} */}
      <nav className="bg-gray-50 px-7 py-2 grid justify-items-end">
        <ul className="">
          <li className="mr-6">
            <Link href='/signin'>
              <button className="bg-transparent hover:bg-slate-200 text-gray-800 font-bold py-1 px-4 rounded">
                sign in
              </button>
            </Link>
          </li>
        </ul>
      </nav>
    </>
  );
};

export default Navbar;
