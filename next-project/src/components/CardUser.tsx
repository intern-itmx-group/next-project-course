"use client";
import {
  Card,
  CardHeader,
  CardBody,
  Typography,
  Button,
} from "@material-tailwind/react";

interface IUser {
  id?: string;
  username?: string;
  password?: string;
  email?: string;
  role?: string;
  imgurl?: string;
}

export const CardUser = (user: IUser) => {
  return (
    <div className="flex justify-center">
      <Card className="w-full max-w-[48rem] flex-row">
        <CardHeader
          shadow={false}
          floated={false}
          className=" w-2/5 shrink-0 rounded-r-none flex justify-center items-center p-8"
        >
          <img
            src={user.imgurl}
            alt="card-image"
            className="h-30 w-30 object-cover rounded-full"
          />
        </CardHeader>
        <CardBody className="flex flex-col justify-center">
          <Typography variant="h6" color="blue" className="mb-6 uppercase">
            you are {user.role}
          </Typography>
          <div className="mb-8">
            <Typography color="gray" className="font-normal">
              username : {user.username}
            </Typography>
            <Typography color="gray" className="font-normal">
              email: {user.email}
            </Typography>
          </div>
        </CardBody>
      </Card>
    </div>
  );
};
