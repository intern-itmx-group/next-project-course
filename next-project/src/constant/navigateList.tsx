export type NavItem = {
    navName: string;
    auth?: string;
    path: string;
  };
  
export const navigateList_admin: NavItem[] = [
    {
      navName: 'home',
      path: '/home',
    },
    {
      navName: 'user',
      path: '/user',
    },
    {
      navName: 'admin',
      path: '/admin',
    },
    {
      navName: 'product',
      path: '/product',
    },
    {
      navName: 'sign out',
      path: '',
    },
  ];
  
  export const navigateList_user: NavItem[] = [
    {
      navName: 'home',
      path: '/home',
    },
    {
      navName: 'user',
      path: '/user',
    },
    {
      navName: 'product',
      path: '/product',
    },
    {
      navName: 'sign out',
      path: '',

    },
  ]
  