import { API_URL } from "@/config";
import axios from "axios";
import { axiosInstance } from "./axiosInstance";

type Product = {
  product_id?: number;
  product_name?: string;
  product_detail?: string;
  product_price?: number;
};

export async function getAllProducts() {
  const response = await axiosInstance.get(`/get_products`);
  const data = response.data;
  // console.log(data);
  return data;
}

// export async function addProduct(product: Product) {
//   const response = await axios.post(
//     `${API_URL}:${process.env.MYPORT}/add_products`,
//     product
//   );
//   const data = response.data;
//   console.log(data.message);
//   return data.message;
// }
