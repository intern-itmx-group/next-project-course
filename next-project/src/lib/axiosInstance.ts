import { API_URL } from "@/config";
import axios from "axios";
import { options } from "../app/api/auth/[...nextauth]/options";
import { getServerSession } from "next-auth";

// const headerToken = async () => {
//   const session = await getServerSession(options);
//   console.log(session?.token);
//   return session ? session.token : null;
// };

// export const axiosInstance = axios.create({
//   baseURL: `${API_URL}:${process.env.MYPORT}`,
//   headers: {
//     Authorization: `Bearer ${headerToken}`,
//   },
// });

export const axiosInstance = axios.create({
  baseURL: `${API_URL}:${process.env.MYPORT}`,
});

axiosInstance.interceptors.request.use(async (config) => {
  const session = await getServerSession(options);

  if (session) {
    config.headers.Authorization = `Bearer ${session.token}`;
  }

  return config;
});
