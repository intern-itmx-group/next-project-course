import jwt from "jsonwebtoken";
import { getServerSession } from "next-auth";
import { options } from "../../src/app/api/auth/[...nextauth]/options";
import { axiosInstance } from "./axiosInstance";

// type Session = {
//   userid: string;
//   expires: string;
//   token: string;
// };

export const getUserApi = async (path: string) => {
  const paths = path.split("/");
  const endpoint_path = paths[paths.length - 1];
  const session = await getServerSession(options);
  const userid = jwt.verify(session?.userid || "", process.env.SECRET_KEY!);

  try {
    switch (endpoint_path) {
      case "users":
        const allUser = await axiosInstance.get(path);
        return allUser.data;
      case "find":
        const oneUser = await axiosInstance.get(`${path}/${userid}`);
        return oneUser.data;
    }
  } catch (error) {
    console.error("Error fetching user data:", error);
    throw error;
  }
};

// export const getAllUser = cache(async (session: any) => {
//   const userid = jwt.verify(session.userid || "", process.env.SECRET_KEY!);
//   const res = await fetch(`http://127.0.0.1:${process.env.MYPORT}/users`, {
//     headers: {
//       Authorization: `Bearer ${session.token}`,
//     },
//   });
//   return res.json();
// });

// export const getUser = cache(async (session: any) => {
//   const userid = jwt.verify(session.userid || "", process.env.SECRET_KEY!);
//   const res = await fetch(
//     `http://127.0.0.1:${process.env.MYPORT}/users/find/${userid}`,
//     {
//       headers: {
//         Authorization: `Bearer ${session.token}`,
//       },
//       cache: "force-cache",
//     }
//   );
//   return res.json();
// });

// export const getUserDetail = cache(async (userid: any) => {
//   const session = await getServerSession(options);
//   const res = await fetch(
//     `http://127.0.0.1:${process.env.MYPORT}/users/find/${userid}`,
//     {
//       headers: {
//         Authorization: `Bearer ${session!.token}`,
//       },
//     }
//   );
//   return res.json();
// });
