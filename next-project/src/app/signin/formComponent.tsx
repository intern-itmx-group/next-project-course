"use client";
import { Button, Input, Typography, Card } from "@material-tailwind/react";
import { signIn } from "next-auth/react";
import { useState } from "react";

export default function Form() {
  const initialValues = { username: "", password: "" };
  const [userLogin, setUserLogin] = useState(initialValues);
  const handleSubmit = async (e: any) => {
    e.preventDefault();

    const res = await signIn("credentials", {
      username: userLogin.username,
      password: userLogin.password,
      callbackUrl: `${window.location.origin}/home`,
    });
  };

  return (
    <Card color="transparent" shadow={false}>
      <Typography variant="h4" color="blue-gray">
        Sign In
      </Typography>
      <Typography color="gray" className="mt-1 font-normal">
        Enter your details to login.
      </Typography>
      <form className="mt-8 mb-2 w-80 max-w-screen-lg sm:w-96">
        <div className="mb-4 flex flex-col gap-6">
          <Input
            size="lg"
            label="Username"
            onChange={({ target }) =>
            setUserLogin({ ...userLogin, username: target.value })
            }
          />
          <Input
            type="password"
            size="lg"
            label="Password"
            onChange={({ target }) =>
            setUserLogin({ ...userLogin, password: target.value })
            }
          />
        </div>
        <Button className="mt-6" fullWidth onClick={handleSubmit}>
          Sign in
        </Button>
      </form>
    </Card>
  );
}