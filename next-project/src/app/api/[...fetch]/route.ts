import { NextResponse, NextRequest } from "next/server";

export async function GET(request: NextRequest) {
  console.log("[req] : pathname", request.nextUrl.pathname);
  console.log("[req] : header", request.headers.get("accept"));

  // If accessed directly in the browser, return an empty response (for HTML requests)
  if (request.headers.get("accept")?.includes("text/html")) {
    return NextResponse.json([]);
  }

  if (request.nextUrl.pathname.includes("/checkrole")) {
    const userId = request.nextUrl.pathname.split("/")[4];
    try {
      const APIResponse = await fetch(
        `http://127.0.0.1:3080/users/checkrole/${userId}`
      );
      const data = await APIResponse.json();

      return NextResponse.json(data);
    } catch (error) {
      console.error("Error fetching data:", error);
      return NextResponse.json({ error: "Internal server error" });
    }
  }

  if (request.nextUrl.pathname.includes("/user")) {
    try {
      const APIResponse = await fetch(
        "https://jsonplaceholder.typicode.com/users"
      );
      const data = await APIResponse.json();

      // Respond with the modified or fake data in JSON format
      return NextResponse.json(data);
    } catch (error) {
      console.error("Error fetching data:", error);
      return NextResponse.json({ error: "Internal server error" });
    }
  }

  return NextResponse.json({
    message: `Hello world! from fetch ${request.nextUrl.pathname}`,
  });
}
