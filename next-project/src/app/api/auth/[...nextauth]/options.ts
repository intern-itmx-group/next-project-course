import type { NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import jwt from "jsonwebtoken";

export const options: NextAuthOptions = {
  providers: [
    CredentialsProvider({
      name: "Credentials",
      credentials: {
        username: { label: "Username", type: "text" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials) {
        const res = await fetch(
          `http://127.0.0.1:${process.env.MYPORT}/login`,
          {
            method: "POST",
            body: JSON.stringify({
              username: credentials?.username,
              password: credentials?.password,
            }),
            headers: { "Content-Type": "application/json" },
          }
        );
        const user = await res.json();
        if (res.ok && user) {
          return user;
        } else {
          return null;
        }
      },
    }),
  ],
  callbacks: {
    async jwt({ token, user }) {
      if (user) {
        token.access = user.access_token;
        // token.expired = user.expired
        token.user = user.user;
      }
      return token;
    },
    async session({ session, token }) {
      if (token) {
        let sessionToken = jwt.sign(
          token?.user?.id || "",
          process.env.SECRET_KEY!
          // { algorithm: "RS256" }
        );
        const expired = new Date();
        expired.setHours(expired.getHours() + 3);
        session = {
          userid: sessionToken,
          expires: expired,
          token: token.access,
        };
      }
      return session;
    },
  },
  pages: {
    signIn: "/signin",
  },
};
