"use client";

import { Spinner } from "@material-tailwind/react";

export default function Loading() {
  return (
    <>
      <div className="h-screen w-full flex flex-col justify-center items-center align-middle">
        <Spinner className="h-12 w-12" />
      </div>
    </>
  );
}
