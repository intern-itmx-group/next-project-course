'use client'
import { Typography } from "@material-tailwind/react";

function Home() {
  return (
    <div>
    <Typography variant="h1" className="animate-bounce">Hi :D</Typography> 
    </div>
  );
}

export default Home;
