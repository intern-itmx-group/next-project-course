import { getUserApi } from "@/lib/getData";
import { CardUser } from "../../../components/CardUser";

async function User() {
  const userInfo = await getUserApi(`/users/find`);
  return (
    <>
      <CardUser
        username={userInfo.username}
        email={userInfo.email}
        role={userInfo.role}
        imgurl={userInfo.imgurl}
      />
      {/* <img
        src={userInfo.imgurl}
        style={{ borderRadius: "50%", maxHeight: "10rem" }}
      />
      <div>username: {userInfo.username}</div>
      <div>email: {userInfo.email}</div> */}
    </>
  );
}

export default User;
