import { EcommerceCard } from "@/components/Card";
import ProductDialog from "@/components/Dialog/ProductDialog";
import { getAllProducts } from "@/lib/getAllProduct";
import React from "react";

type Product = {
  product_id: number;
  product_name: string;
  product_detail: string;
  product_price: number;
};

const ProductPage = async () => {
  const products: Promise<Product[]> = await getAllProducts();
  return (
    <div>
      <h2 className="text-center font-bold text-xl my-4">Product List</h2>
      <ProductDialog/>
        <div className="grid grid-cols-3 gap-4">
          {products &&
            (await products).map((product: any) => (
              <div key={product.product_id} className="flex justify-center">
                <EcommerceCard product={product} />
              </div>
            ))}
        </div>
    </div>
  );
};

export default ProductPage;
