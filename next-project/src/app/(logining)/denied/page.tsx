import Link from "next/link";

export default function Denied() {
  return (
    <section className="flex flex-col gap-12 items-center mt-5">
      <h1 className="text-3xl">Access Denied</h1>
      <p className="text-xl max-w-2xl text-center">
        You are logged in, but you do not have the required access level to view
        this page.
      </p>
      <Link href="/home" className="text-2xl underline">
        Go to home page
      </Link>
    </section>
  );
}
