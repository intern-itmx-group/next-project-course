import "tailwindcss/tailwind.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import NavbarRole from "@/components/NavbarRole";
import { getUserApi } from "@/lib/getData";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Create Next App",
  description: "Generated by create next app",
};

export default async function Layout({
  children,
}: {
  children: React.ReactNode;
}) {
  const userInfo = await getUserApi(`/users/find`);
  console.log(userInfo);

  return (
    <div className="flex flex-col w-full">
        <NavbarRole role={userInfo.role} />
        <main className="grid mx-8 my-6">{children}</main>
    </div>
  );
}
