import { getUserApi } from "@/lib/getData";
import { AdminTable } from "@/components/AdminTable";

async function Admin() {
  const userInfo = await getUserApi(`/users`);
  return (<div className="h-full w-full">
    {userInfo && <AdminTable data={userInfo} />}
    </div>);
}

export default Admin;
